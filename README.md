# bpm-package-blueprint

This is a development tool for the Blockchain Package Manager by [Blockdaemon](https://blockdaemon.com/). Deploy, maintain, and upgrade blockchain nodes on your own infrastructure.

Further reading:

* End-user documentation: https://cli.bpm.docs.blockdaemon.com/
* Developer documentation: https://sdk.bpm.docs.blockdaemon.com/

# Overview

`bpm-package-blueprint` sets up a simple directory structure with:

* Example code to get started
* Linter configuration
* Build scripts and configuration
* LICENSE.txt and README.md files

It currently works with gitlab repositories only!

To create a new bpm package, follow these steps:

1. Clone this repository: git clone git@gitlab.com:Blockdaemon/bpm-package-blueprint.git `~/projects/bpm-package-blueprint`
2. Create a new directory for your package: `mkdir -p ~/projects/<plugin-name>` 
3. Change your working directory to the new package directory: `cd ~/projects/<plugin-name>`
4. Copy configuration file into your package: `cp ../bpm-package-blueprint/.env-blueprint.env .`
5. Edit the values in your copy of `.env-blueprint.env`
6. Run (while still in your new package directory): `../bpm-package-blueprint/initialize-plugin.sh`
7. Build the project: `make build`
