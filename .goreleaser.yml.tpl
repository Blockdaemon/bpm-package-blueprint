project_name: bpm-$PLUGIN_NAME
before:
  hooks:
    - go mod download # Download once at the start because goreleaser builds in parallel (https://goreleaser.com/customization/)
builds:
- main: ./cmd
  ldflags:
    - -X main.version={{.Version}}
  env:
    - CGO_ENABLED=0
  goos:
    - darwin
    - linux
  goarch:
    - amd64
  binary: $PLUGIN_NAME
signs:
  - artifacts: all
    args: ["-u", "$SIGNING_KEY_IDENTIFIER", "--output", "${signature}", "--detach-sign", "${artifact}"]
changelog:
  sort: asc
  filters:
    exclude:
    - '^docs:'
    - '^test:'
release:
  gitlab:
    owner: $GITLAB_OWNER
    name: $GITLAB_REPO

