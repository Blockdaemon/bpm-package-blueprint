#!/bin/bash

set -e

source .blueprint-env

SUBST_VARS='$PLUGIN_NAME:$MAINTAINER:$MAINTAINER_HOMEPAGE:$MAINTAINER_EMAIL:$SIGNING_KEY_IDENTIFIER:$GITLAB_OWNER:$GITLAB_REPO'

function render_template {
	if [[ -f $1 ]]; then
		echo "Found \"$1\", skipping rendering template"
	else
		echo "Rendering $1"
		SOURCE_DIR=$(dirname "$0")
		cat "$SOURCE_DIR/$1.tpl" | envsubst "$SUBST_VARS"  > $1
	fi
}

render_template README.md
render_template LICENSE.txt
render_template Makefile
render_template .gitignore
render_template .gitlab-ci.yml
render_template .goreleaser.yml

mkdir -p cmd
render_template cmd/main.go
render_template cmd/templates.go
render_template cmd/parameter_validator.go

if [[ -f "go.mod" ]]; then
    echo "Found \"go.mod\" file, skipping \"go mod init\""
else
	go mod init gitlab.com/${GITLAB_OWNER}/${GITLAB_REPO} 
fi

