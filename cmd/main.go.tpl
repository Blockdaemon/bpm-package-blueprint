// TODO: validation
package main

import (
	"go.blockdaemon.com/bpm/sdk/pkg/plugin"
	"go.blockdaemon.com/bpm/sdk/pkg/docker"
)

// The version gets populated during the build process
var version string

func main() {
	// Define your configuration files here (the templates are in `cmd/templates.go`)
	templates := map[string]string{
		"configs/example_config.txt":  exampleConfigTemplate,
	}

	// Define your containers here
	containers := []docker.Container{ }

	// Define your own customer parameters here (nearly every plugin has `subtype` and `network` as a convention)
	parameters := []plugin.Parameter{
		{
			Name:        "subtype",
			Type:        plugin.ParameterTypeString,
			Description: "The type of node. Can be either `watcher` or `validator`",
			Mandatory:   false,
			Default:     "watcher",
		},
		{
			Name:        "network",
			Type:        plugin.ParameterTypeString,
			Description: "The network. Can be either `mainnet` or `testnet`",
			Mandatory:   false,
			Default:     "mainnet",
		},
	}

	name := "${PLUGIN_NAME}"
	description := "A $PLUGIN_NAME package"

	${PLUGIN_NAME}Plugin := plugin.NewDockerPlugin(name, version, description, parameters, templates, containers)

	// Add custom validator
	${PLUGIN_NAME}Plugin.ParameterValidator = New${PLUGIN_NAME}ParameterValidator(${PLUGIN_NAME}Plugin.Meta().Parameters)

	plugin.Initialize(${PLUGIN_NAME}Plugin)
}
