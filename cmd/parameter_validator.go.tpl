package main

import (
	"fmt"

	"go.blockdaemon.com/bpm/sdk/pkg/node"
	"go.blockdaemon.com/bpm/sdk/pkg/plugin"
)

// ${PLUGIN_NAME}ParameterValidator validates parameters
type ${PLUGIN_NAME}ParameterValidator struct {
	plugin.SimpleParameterValidator
}

// ValidateParameters uses SimpleParameterValidator but also check is the network is correct
func (t ${PLUGIN_NAME}ParameterValidator) ValidateParameters(currentNode node.Node) error {
	// Call SimpleParameterValidator to check if all parameters actually have values
	if err := t.SimpleParameterValidator.ValidateParameters(currentNode); err != nil {
		return err
	}

	network := currentNode.StrParameters["network"]
	if network != "mainnet" && network != "testnet" {
		return fmt.Errorf("unknown network: %q", network)
	}

	subtype := currentNode.StrParameters["subtype"]
	if subtype != "watcher" && network != "validator" {
		return fmt.Errorf("unknown subtype: %q", subtype)
	}

	return nil
}

// New${PLUGIN_NAME}ParameterValidator creates a new instance of ${PLUGIN_NAME}ParameterValidator
func New${PLUGIN_NAME}ParameterValidator(pluginParameters []plugin.Parameter) ${PLUGIN_NAME}ParameterValidator {
	return ${PLUGIN_NAME}ParameterValidator{
		SimpleParameterValidator: plugin.NewSimpleParameterValidator(pluginParameters),
	}
}
