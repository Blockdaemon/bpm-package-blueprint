package main

const (
    // Define your own templates for configuration files here
    exampleConfigTemplate = `# This is an example configuration file to show how variable substitution works

Node ID: {{ .Node.ID }}
Subtype: {{ .Node.StrParameters.subtype }}
Network: {{ .Node.StrParameters.network }}
`
)




